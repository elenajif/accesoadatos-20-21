package gui;

import util.Util;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.*;
import java.util.Vector;


public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {

    private Modelo modelo;
    private Vista vista;
    boolean refrescar;

    /**
     * Constructor de la clase, inicializa el modelo y la vista, carga los datos
     * desde un fichero, añade los listeners y lista los datos.
     *
     * @param modelo Modelo
     * @param vista  Vista
     */
    public Controlador(Modelo modelo, Vista vista) {
        this.modelo = modelo;
        this.vista = vista;
        modelo.conectar();
        setOptions();
        addActionListeners(this);
        addItemListeners(this);
        addWindowListeners(this);
        refrescarTodo();
    }

    private void refrescarTodo() {
        refrescarAutores();
        refrescarEditorial();
        refrescarLibros();
        refrescar = false;
    }

    /**
     * Añade ActionListeners a los botones y menus de la vista
     * @param listener ActionListener que se añade
     */
    private void addActionListeners(ActionListener listener) {
        vista.btnLibrosAnadir.addActionListener(listener);
        vista.btnAutoresAnadir.addActionListener(listener);
        vista.btnEditorialesAnadir.addActionListener(listener);
        vista.btnLibrosEliminar.addActionListener(listener);
        vista.btnAutoresEliminar.addActionListener(listener);
        vista.btnEditorialesEliminar.addActionListener(listener);
        vista.btnLibrosModificar.addActionListener(listener);
        vista.btnAutoresModificar.addActionListener(listener);
        vista.btnEditorialesModificar.addActionListener(listener);
        vista.optionDialog.btnOpcionesGuardar.addActionListener(listener);
        vista.itemOpciones.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btnValidate.addActionListener(listener);
    }

    /**
     * Añade WindowListeners a la vista
     * @param listener WindowListener que se añade
     */
    private void addWindowListeners(WindowListener listener) {
        vista.addWindowListener(listener);
    }

    /**
     * Muestra los atributos de un objeto seleccionado y los borra una vez se deselecciona
     * @param e Evento producido en una lista
     */

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()
                && !((ListSelectionModel) e.getSource()).isSelectionEmpty()) {
            if (e.getSource().equals(vista.editorialesTabla.getSelectionModel())) {
                int row = vista.editorialesTabla.getSelectedRow();
                vista.txtNombreEditorial.setText(String.valueOf(vista.editorialesTabla.getValueAt(row, 1)));
                vista.txtEmail.setText(String.valueOf(vista.editorialesTabla.getValueAt(row, 2)));
                vista.txtTelefono.setText(String.valueOf(vista.editorialesTabla.getValueAt(row, 3)));
                vista.comboTipoEditorial.setSelectedItem(String.valueOf(vista.editorialesTabla.getValueAt(row, 4)));
                vista.txtWeb.setText(String.valueOf(vista.editorialesTabla.getValueAt(row, 5)));
            } else if (e.getSource().equals(vista.autoresTabla.getSelectionModel())) {
                int row = vista.autoresTabla.getSelectedRow();
                vista.txtNombre.setText(String.valueOf(vista.autoresTabla.getValueAt(row, 1)));
                vista.txtApellidos.setText(String.valueOf(vista.autoresTabla.getValueAt(row, 2)));
                vista.fechaNacimiento.setDate((Date.valueOf(String.valueOf(vista.autoresTabla.getValueAt(row, 3)))).toLocalDate());
                vista.txtPais.setText(String.valueOf(vista.autoresTabla.getValueAt(row, 4)));
            } else if (e.getSource().equals(vista.librosTabla.getSelectionModel())) {
                int row = vista.librosTabla.getSelectedRow();
                vista.txtTitulo.setText(String.valueOf(vista.librosTabla.getValueAt(row, 1)));
                vista.comboAutor.setSelectedItem(String.valueOf(vista.librosTabla.getValueAt(row, 5)));
                vista.comboEditorial.setSelectedItem(String.valueOf(vista.librosTabla.getValueAt(row, 3)));
                vista.comboGenero.setSelectedItem(String.valueOf(vista.librosTabla.getValueAt(row, 4)));
                vista.fecha.setDate((Date.valueOf(String.valueOf(vista.librosTabla.getValueAt(row, 7)))).toLocalDate());
                vista.txtIsbn.setText(String.valueOf(vista.librosTabla.getValueAt(row, 2)));
                vista.txtPrecioLibro.setText(String.valueOf(vista.librosTabla.getValueAt(row, 6)));
            } else if (e.getValueIsAdjusting()
                    && ((ListSelectionModel) e.getSource()).isSelectionEmpty() && !refrescar) {
                if (e.getSource().equals(vista.editorialesTabla.getSelectionModel())) {
                    borrarCamposEditoriales();
                } else if (e.getSource().equals(vista.autoresTabla.getSelectionModel())) {
                    borrarCamposAutores();
                } else if (e.getSource().equals(vista.librosTabla.getSelectionModel())) {
                    borrarCamposLibros();
                }
            }
        }
    }
    /**
     * Añade sus respectivas acciones a los botones de añadir, modificar y eliinar de las listas.
     * Añade sus respectivas acciones a los menus de guardar, guardar como y mostrar opciones.
     * Añade la acción de guardar las opciones al dialog de opciones.
     * Añade las acciones de guardar, no guardar y cancelar a los botones del dialog de guardar
     *  cambios cuando se intenta salir de la aplicación una vez se ha desactivado el autoguardado.
     * @param e Evento producido al pulsar un botón o menu
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        switch (command) {
            case "Opciones":
                vista.adminPasswordDialog.setVisible(true);
                break;
            case "Desconectar":
                modelo.desconectar();
                break;
            case "Salir":
                System.exit(0);
                break;
            case "abrirOpciones":
                if(String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPassword())) {
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                } else {
                    Util.showErrorAlert("La contraseña introducida no es correcta.");
                }
                break;
            case "guardarOpciones":
                modelo.setPropValues(vista.optionDialog.tfIP.getText(), vista.optionDialog.tfUser.getText(),
                        String.valueOf(vista.optionDialog.pfPass.getPassword()), String.valueOf(vista.optionDialog.pfAdmin.getPassword()));
                vista.optionDialog.dispose();
                vista.dispose();
                new Controlador(new Modelo(), new Vista());
                break;
            case "anadirLibro": {
                try {
                    if (comprobarLibroVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.librosTabla.clearSelection();
                    } else if (modelo.libroIsbnYaExiste(vista.txtIsbn.getText())) {
                        Util.showErrorAlert("Ese ISBN ya existe.\nIntroduce un libro diferente");
                        vista.librosTabla.clearSelection();
                    } else {
                        modelo.insertarLibro(
                                vista.txtTitulo.getText(),
                                vista.txtIsbn.getText(),
                                String.valueOf(vista.comboEditorial.getSelectedItem()),
                                String.valueOf(vista.comboGenero.getSelectedItem()),
                                String.valueOf(vista.comboAutor.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecioLibro.getText()),
                                vista.fecha.getDate());
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.librosTabla.clearSelection();
                }
                borrarCamposLibros();
                refrescarLibros();
            }
            break;
            case "modificarLibro": {
                try {
                    if (comprobarLibroVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.librosTabla.clearSelection();
                    } else {
                        modelo.modificarLibro(
                                vista.txtTitulo.getText(),
                                vista.txtIsbn.getText(),
                                String.valueOf(vista.comboEditorial.getSelectedItem()),
                                String.valueOf(vista.comboGenero.getSelectedItem()),
                                String.valueOf(vista.comboAutor.getSelectedItem()),
                                Float.parseFloat(vista.txtPrecioLibro.getText()),
                                vista.fecha.getDate(),
                                Integer.parseInt((String)vista.librosTabla.getValueAt(vista.librosTabla.getSelectedRow(), 0)));
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.librosTabla.clearSelection();
                }
                borrarCamposLibros();
                refrescarLibros();
            }
            break;
            case "eliminarLibro":
                modelo.borrarLibro(Integer.parseInt((String)vista.librosTabla.getValueAt(vista.librosTabla.getSelectedRow(), 0)));
                borrarCamposLibros();
                refrescarLibros();
                break;
            case "anadirAutor": {
                try {
                    if (comprobarAutorVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.autoresTabla.clearSelection();
                    } else if (modelo.autorNombreYaExiste(vista.txtNombre.getText(),
                            vista.txtApellidos.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce un autor diferente");
                        vista.autoresTabla.clearSelection();
                    } else {
                        modelo.insertarAutor(vista.txtNombre.getText(),
                                vista.txtApellidos.getText(),
                                vista.fechaNacimiento.getDate(),
                                vista.txtPais.getText());
                        refrescarAutores();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.autoresTabla.clearSelection();
                }
                borrarCamposAutores();
            }
            break;
            case "modificarAutor": {
                try {
                    if (comprobarAutorVacio()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.autoresTabla.clearSelection();
                    } else {
                        modelo.modificarAutor(vista.txtNombre.getText(), vista.txtApellidos.getText(),
                                vista.fechaNacimiento.getDate(), vista.txtPais.getText(),
                                Integer.parseInt((String)vista.autoresTabla.getValueAt(vista.autoresTabla.getSelectedRow(), 0)));
                        refrescarAutores();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.autoresTabla.clearSelection();
                }
                borrarCamposAutores();
            }
            break;
            case "eliminarAutor":
                modelo.borrarAutor(Integer.parseInt((String)vista.autoresTabla.getValueAt(vista.autoresTabla.getSelectedRow(), 0)));
                borrarCamposAutores();
                refrescarAutores();
                break;
            case "anadirEditorial": {
                try {
                    if (comprobarEditorialVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.editorialesTabla.clearSelection();
                    } else if (modelo.editorialNombreYaExiste(vista.txtNombreEditorial.getText())) {
                        Util.showErrorAlert("Ese nombre ya existe.\nIntroduce una editorial diferente.");
                        vista.editorialesTabla.clearSelection();
                    } else {
                        modelo.insertarEditorial(vista.txtNombreEditorial.getText(), vista.txtEmail.getText(),
                                vista.txtTelefono.getText(),
                                (String) vista.comboTipoEditorial.getSelectedItem(),
                                vista.txtWeb.getText());
                        refrescarEditorial();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.editorialesTabla.clearSelection();
                }
                borrarCamposEditoriales();
            }
            break;
            case "modificarEditorial": {
                try {
                    if (comprobarEditorialVacia()) {
                        Util.showErrorAlert("Rellena todos los campos");
                        vista.editorialesTabla.clearSelection();
                    } else {
                        modelo.modificarEditorial(vista.txtNombreEditorial.getText(), vista.txtEmail.getText(), vista.txtTelefono.getText(),
                                String.valueOf(vista.comboTipoEditorial.getSelectedItem()), vista.txtWeb.getText(),
                                Integer.parseInt((String)vista.editorialesTabla.getValueAt(vista.editorialesTabla.getSelectedRow(), 0)));
                        refrescarEditorial();
                    }
                } catch (NumberFormatException nfe) {
                    Util.showErrorAlert("Introduce números en los campos que lo requieren");
                    vista.editorialesTabla.clearSelection();
                }
                borrarCamposEditoriales();
            }
            break;
            case "eliminarEditorial":
                modelo.borrarEditorial(Integer.parseInt((String)vista.editorialesTabla.getValueAt(vista.editorialesTabla.getSelectedRow(), 0)));
                borrarCamposEditoriales();
                refrescarEditorial();
                break;
        }
    }

    /**
     * Añade acciones cuando la ventana se cierra. Si el autoguardado está activado la ventana se cerrará
     * sin más, volcando todos los datos en el fichero. De lo contrario pueden ocurrir dos cosas:
     * Si todos los cambios han sido guardados, la ventana se cerrará. Si no lo están, aparecerá una ventana
     * que permitirá al usuario decidir que hacer con esos cambios no guardados o bien cancelar el cierre
     * de la aplicación.
     * @param e Evento producido al tratar de cerrar la ventana
     */
    @Override
    public void windowClosing(WindowEvent e) {
            System.exit(0);
    }
    /**
     * Actualiza las editoriales que se ven en la lista y los comboboxes
     */
    private void refrescarEditorial() {
        try {
            vista.editorialesTabla.setModel(construirTableModelEditoriales(modelo.consultarEditorial()));
            vista.comboEditorial.removeAllItems();
            for(int i = 0; i < vista.dtmEditoriales.getRowCount(); i++) {
                vista.comboEditorial.addItem(vista.dtmEditoriales.getValueAt(i, 0)+" - "+
                        vista.dtmEditoriales.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private  DefaultTableModel construirTableModelEditoriales(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmEditoriales.setDataVector(data, columnNames);

        return vista.dtmEditoriales;

    }

    private void refrescarAutores() {
        try {
            vista.autoresTabla.setModel(construirTableModeloAutores(modelo.consultarAutor()));
            vista.comboAutor.removeAllItems();
            for(int i = 0; i < vista.dtmAutores.getRowCount(); i++) {
                vista.comboAutor.addItem(vista.dtmAutores.getValueAt(i, 0)+" - "+
                        vista.dtmAutores.getValueAt(i, 2)+", "+vista.dtmAutores.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModeloAutores(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmAutores.setDataVector(data, columnNames);

        return vista.dtmAutores;

    }

    /**
     * Actualiza los libros que se ven en la lista y los comboboxes
     */
    private void refrescarLibros() {
        try {
            vista.librosTabla.setModel(construirTableModelLibros(modelo.consultarLibros()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelLibros(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmLibros.setDataVector(data, columnNames);

        return vista.dtmLibros;

    }

    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    /**
     * Setea el dialog de opciones según las opciones que el usuario guardo en la
     * última ejecución del programa.
     */
    private void setOptions() {
        vista.optionDialog.tfIP.setText(modelo.getIP());
        vista.optionDialog.tfUser.setText(modelo.getUser());
        vista.optionDialog.pfPass.setText(modelo.getPassword());
        vista.optionDialog.pfAdmin.setText(modelo.getAdminPassword());
    }

    /**
     * Vacía los campos de la tab de libros
     */
    private void borrarCamposLibros() {
        vista.comboEditorial.setSelectedIndex(-1);
        vista.comboAutor.setSelectedIndex(-1);
        vista.txtTitulo.setText("");
        vista.txtIsbn.setText("");
        vista.comboGenero.setSelectedIndex(-1);
        vista.txtPrecioLibro.setText("");
        vista.fecha.setText("");
    }

    /**
     * Vacía los campos de la tab de autores
     */
    private void borrarCamposAutores() {
        vista.txtNombre.setText("");
        vista.txtApellidos.setText("");
        vista.txtPais.setText("");
        vista.fechaNacimiento.setText("");
    }

    /**
     * Vacía los campos de la tab de editoriales
     */
    private void borrarCamposEditoriales() {
        vista.txtNombreEditorial.setText("");
        vista.txtEmail.setText("");
        vista.txtTelefono.setText("");
        vista.comboTipoEditorial.setSelectedIndex(-1);
        vista.txtWeb.setText("");
    }

    /**
     * Comprueba que los campos necesarios para añadir un libro estén vacíos
     *
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarLibroVacio() {
        return vista.txtTitulo.getText().isEmpty() ||
                vista.txtPrecioLibro.getText().isEmpty() ||
                vista.txtIsbn.getText().isEmpty() ||
                vista.comboGenero.getSelectedIndex() == -1 ||
                vista.comboAutor.getSelectedIndex() == -1 ||
                vista.comboEditorial.getSelectedIndex() == -1 ||
                vista.fecha.getText().isEmpty();
    }

    /**
     * Comprueba que los campos necesarios para añadir un autor estén vacíos
     *
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarAutorVacio() {
        return vista.txtApellidos.getText().isEmpty() ||
                vista.txtNombre.getText().isEmpty() ||
                vista.txtPais.getText().isEmpty() ||
                vista.fechaNacimiento.getText().isEmpty();
    }

    /**
     * Comprueba que los campos necesarios para añadir una editorial estén vacíos
     *
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarEditorialVacia() {
        return vista.txtNombreEditorial.getText().isEmpty() ||
                vista.txtEmail.getText().isEmpty() ||
                vista.txtTelefono.getText().isEmpty() ||
                vista.comboTipoEditorial.getSelectedIndex() == -1 ||
                vista.txtWeb.getText().isEmpty();
    }

       /*LISTENERS IPLEMENTOS NO UTILIZADOS*/

    private void addItemListeners(Controlador controlador) {
    }

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosed(WindowEvent e) {
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }
}
