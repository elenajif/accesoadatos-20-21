package gui;

import base.enums.*;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Vista extends JFrame{

    private final static String TITULOFRAME = "BBDDVariasTablas";
    private JPanel panel1;
    private JTabbedPane tabbedPane;
    /*AUTOR*/
    JTextField txtNombre;
    JTextField txtApellidos;
    JTextField txtPais;
    JTable autoresTabla;
    JButton btnAutoresAnadir;
    JButton btnAutoresModificar;
    JButton btnAutoresEliminar;
    DatePicker fechaNacimiento;
    /*EDITORIAL*/
    JTextField txtNombreEditorial;
    JTextField txtEmail;
    JTextField txtTelefono;
    JTable editorialesTabla;
    JButton btnEditorialesEliminar;
    JButton btnEditorialesAnadir;
    JButton btnEditorialesModificar;
    JComboBox<String> comboTipoEditorial;
    JTextField txtWeb;
    /*BOOK*/
    JTextField txtTitulo;
    JComboBox<String> comboAutor;
    JComboBox<String> comboEditorial;
    JComboBox<String> comboGenero;
    JTextField txtIsbn;
    JTextField txtPrecioLibro;
    JTable librosTabla;
    JButton btnLibrosEliminar;
    JButton btnLibrosAnadir;
    JButton btnLibrosModificar;
    DatePicker fecha;
    /*SEARCH*/
    JLabel etiquetaEstado;
    /*DEFAULT TABLE MODELS*/
    DefaultTableModel dtmEditoriales;
    DefaultTableModel dtmAutores;
    DefaultTableModel dtmLibros;
    /*MENUBAR*/
    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;
    /*OPTION DIALOG*/
    OptionDialog optionDialog;
    /*SAVECHANGESDIALOG*/
    JDialog adminPasswordDialog;
    JButton btnValidate;
    JPasswordField adminPassword;

    /**
     * Constructor de la clase.
     * Setea el título de la ventana y llama al metodo que la inicia
     */
    public Vista() {
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Configura la ventana y la hace visible
     */
    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        this.setLocationRelativeTo(null);
        optionDialog = new OptionDialog(this);
        setMenu();
        setAdminDialog();
        setEnumComboBox();
        setTableModels();
    }

    /**
     * Inicializa los DefaultTableModel, los setea en sus respectivas tablas
     */
    private void setTableModels() {

        this.dtmLibros = new DefaultTableModel();
        this.librosTabla.setModel(dtmLibros);

        this.dtmAutores = new DefaultTableModel();
        this.autoresTabla.setModel(dtmAutores);

        this.dtmEditoriales = new DefaultTableModel();
        this.editorialesTabla.setModel(dtmEditoriales);
    }

    /**
     * Setea una barra de menus
     */
    private void setMenu(){
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        itemOpciones = new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");
        itemDesconectar = new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemOpciones);
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        mbBar.add(menu);
        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);
    }

    /**
     * Setea los items de cbEType, de cbBGenre y de cbTables con sus respectivas enumeraciones
     */
    private void setEnumComboBox() {
        for(TiposEditoriales constant : TiposEditoriales.values()) { comboTipoEditorial.addItem(constant.getValor()); }
        comboTipoEditorial.setSelectedIndex(-1);

        for(GenerosLibros constant : GenerosLibros.values()) { comboGenero.addItem(constant.getValor()); }
        comboGenero.setSelectedIndex(-1);
    }

    /**
     * Setea un JDialog para introducir la contreña de administrador y poder configurar la base de datos
     */
    private void setAdminDialog() {
        btnValidate = new JButton("Validar");
        btnValidate.setActionCommand("abrirOpciones");
        adminPassword = new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100, 26));
        Object[] options = new Object[] {adminPassword, btnValidate};
        JOptionPane jop = new JOptionPane("Introduce la contraseña"
                , JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options);

        adminPasswordDialog = new JDialog(this, "Opciones", true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);

    }
}
