package com.elenajif.vehiculosmvc.gui;

import com.elenajif.vehiculosmvc.base.Vehiculo;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

/**
 * Created by DAM on 13/11/2020.
 */
public class Ventana {
    private JPanel panel1;
    public JFrame frame;
    public JRadioButton cocheRadioButton;
    public JRadioButton motoRadioButton;
    public JLabel Matricula;
    public JTextField matriculaTxt;
    public JLabel Marca;
    public JTextField marcaTxt;
    public JLabel Modelo;
    public JTextField modeloTxt;
    public JLabel fechamatriculacion;
    public JTextField kmsPlazasTxt;
    public JButton nuevoBtn;
    public JButton exportarBtn;
    public JButton importarBtn;
    public JList list1;
    public DatePicker fechaMatriculacionDPicker;
    public JLabel plazasKmsLbl;

    //Elementos creados por mi
    public DefaultListModel<Vehiculo> dlmVehiculo;

    public Ventana() {
        frame = new JFrame("Vehiculos MVC");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComponents();
    }

    private void initComponents() {
        dlmVehiculo=new DefaultListModel<Vehiculo>();
        list1.setModel(dlmVehiculo);
    }


}
