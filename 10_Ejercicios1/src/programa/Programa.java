package programa;

import clases.GestorTrabajos;

public class Programa {

	public static void main(String[] args) {
		
		System.out.println("1.- Dar de alta un gestor de trabajos");
		GestorTrabajos gestor = new GestorTrabajos();
		
		System.out.println("2.- Dar de alta 3 responsables");
		gestor.altaResponsable("11111", "responsable1");
		gestor.altaResponsable("22222", "responsable2");
		gestor.altaResponsable("33333", "responsable3");
		
		System.out.println("3.- Listar responsables");
		gestor.listarResponsables();
		
		System.out.println("4.- Buscar responsable");
		System.out.println(gestor.buscarResponsable("11111"));
		System.out.println(gestor.buscarResponsable("111112222"));
		
		System.out.println("5.- Dar de alta 3 trabajos");
		gestor.altaTrabajo("trabajo1", "cliente1", 10.0, "2019-04-02");
		gestor.altaTrabajo("trabajo2", "cliente2", 10.0, "2019-05-02");
		gestor.altaTrabajo("trabajo3", "cliente3", 10.0, "2020-06-02");
		gestor.listarTrabajos();
		
		System.out.println("6.- Asignar responsable");
		gestor.asignarResponsable("11111", "trabajo1");
		gestor.asignarResponsable("11111", "trabajo2");
		
		System.out.println("7.- Listar trabajos de responsables");
		gestor.listarTrabajosDeResponsable("11111");
		
		System.out.println("8.- Listar trabajos por a�o");
		gestor.listarTrabajosAnno(2019);
		
		System.out.println("9.- Eliminar trabajo");
		gestor.eliminarTrabajo("trabajo2");
		gestor.listarTrabajos();
		
		System.out.println("10.- Listar trabajos de responsables");
		gestor.listarTrabajosDeResponsable("11111");
		
		System.out.println("11.- Ere");
		gestor.ere();
		gestor.listarResponsables();
	
	}

}
