package com.elenajif.vehiculosbbdd.gui;

/**
 * Created by DAM on 11/12/2020.
 */
import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Profesor on 02/12/2019.
 */
public class Controlador implements ActionListener, TableModelListener{
    private Vista vista;
    private Modelo modelo;

    private enum tipoEstado {conectado, desconectado};
    private tipoEstado estado;

    public Controlador(Vista vista, Modelo modelo) {
        this.modelo = modelo;
        this.vista = vista;
        estado = tipoEstado.desconectado;

        iniciarTabla();
        iniciarTabla1();
        addActionListener(this);
        addTableModelListeners(this);
    }

    private void addActionListener(ActionListener listener){
        vista.btnBuscar.addActionListener(listener);
        vista.btnEliminar.addActionListener(listener);
        vista.btnNuevo.addActionListener(listener);
        vista.btncochesMarca.addActionListener(listener);
        vista.itemConectar.addActionListener(listener);
        vista.itemCrearTabla.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
    }

    private void addTableModelListeners(TableModelListener listener){
        vista.dtm.addTableModelListener(listener);
        vista.dtm1.addTableModelListener(listener);
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        if (e.getType() ==TableModelEvent.UPDATE) {
            System.out.println("actualizada");
            int filaModicada=e.getFirstRow();

            try {
                modelo.modificarVehiculo((Integer)vista.dtm.getValueAt(filaModicada,0),(String)vista.dtm.getValueAt(filaModicada,1),(String)vista.dtm.getValueAt(filaModicada,2),(String)vista.dtm.getValueAt(filaModicada,3),(java.sql.Timestamp)vista.dtm.getValueAt(filaModicada,4));
                vista.lblAccion.setText("Columna actualizada");
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch(comando){
            case "Coches por Marca":
                try {
                    modelo.cochesPorMarca();
                    cargarFilas1(modelo.obtenerDatos1());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }


                break;

            case "CrearTablaCoches":
                try {
                    modelo.crearTablaCoches();
                    vista.lblAccion.setText("Tabla coches creada");
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }

                break;
            case "Nuevo":
                try {
                    modelo.insertarVehiculo(vista.txtMatricula.getText(),vista.txtMarca.getText(),vista.txtModelo.getText(),vista.dateTimePicker.getDateTimePermissive());
                    cargarFilas(modelo.obtenerDatos());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;

            case "Buscar":
                break;
            case "Eliminar":
                try {
                int filaBorrar=vista.tabla.getSelectedRow();
                int idBorrar= (Integer) vista.dtm.getValueAt(filaBorrar,0);
                modelo.eliminarVehiculo(idBorrar);
                vista.dtm.removeRow(filaBorrar);
                vista.lblAccion.setText("Fila Eliminada");

                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;

            case "Salir":
                System.exit(0);
                break;

            case "Conectar":
                if (estado==tipoEstado.desconectado)  {
                    try {
                        modelo.conectar();
                        vista.itemConectar.setText("Desconectar");
                        estado=tipoEstado.conectado;
                        cargarFilas(modelo.obtenerDatos());
                    } catch (SQLException e1) {
                        JOptionPane.showMessageDialog(null,"Error de conexión","Error",JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();
                    }

            } else {
                    try {
                        modelo.desconectar();
                        vista.itemConectar.setText("Conectar");
                        estado=tipoEstado.desconectado;
                        vista.lblAccion.setText("Desconectado");
                    } catch (SQLException e1) {
                        JOptionPane.showMessageDialog(null,"Error de desconexión","Error",JOptionPane.ERROR_MESSAGE);
                        e1.printStackTrace();
                    }


                }
                break;
        }
    }

    private void iniciarTabla(){
        String[] headers={"id","matricula","marca","modelo","fecha matriculacion"};
        vista.dtm.setColumnIdentifiers(headers);
    }

    private void cargarFilas(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[5];
        vista.dtm.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);

            vista.dtm.addRow(fila);
        }

        if(resultSet.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow()+" filas cargadas");
        }


    }

    private void iniciarTabla1(){
        String[] headers={"Cuantos","marca"};
        vista.dtm1.setColumnIdentifiers(headers);
    }

    private void cargarFilas1(ResultSet resultSet) throws SQLException {
        Object[] fila=new Object[2];
        vista.dtm1.setRowCount(0);

        while (resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);

            vista.dtm1.addRow(fila);
        }

        if(resultSet.last()) {
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow()+" filas cargadas");
        }


    }
}