package com.elenajif.vehiculosbbdd.gui;

/**
 * Created by DAM on 11/12/2020.
 */

public class Principal {
    public static void main(String args[]) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}