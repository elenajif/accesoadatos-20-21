package programa;

import clases.Cuenta;
import clases.CuentaAhorroFija;
import clases.CuentaCorriente;
import clases.CuentaPlanPensiones;

public class PrincipalCuentas {

	public static void main(String[] args) {
		System.out.println("Creamos una cuenta");
		Cuenta c1 = new Cuenta();
		System.out.println("Muestro datos");
		System.out.println(c1);
		System.out.println("Creamos una cuenta con datos");
		Cuenta c2 = new Cuenta("1111111111","titular1",11,1);
		System.out.println("Muestro datos");
		System.out.println(c2);

		System.out.println("Creamos una cuenta corriente");
		CuentaCorriente c3 = new CuentaCorriente();
		System.out.println("Muestro datos");
		System.out.println(c3);
		System.out.println("Creamos una cuenta corriente con datos");
		CuentaCorriente c4 = new CuentaCorriente("2222222222","titular2",22,2);
		System.out.println("Muestro datos");
		System.out.println(c4);
		
		System.out.println("Creamos una cuenta ahorro fija");
		CuentaAhorroFija c5 = new CuentaAhorroFija();
		System.out.println("Muestro datos");
		System.out.println(c5);
		System.out.println("Creamos una cuenta ahorro fija con datos");
		CuentaAhorroFija c6 = new CuentaAhorroFija("333333333","titular3",33,3);
		System.out.println("Muestro datos");
		System.out.println(c6);
		
		System.out.println("Creamos una cuenta plan pensiones");
		CuentaPlanPensiones c7 = new CuentaPlanPensiones();
		System.out.println("Muestro datos");
		System.out.println(c7);
		System.out.println("Creamos una cuenta plan pensiones con datos");
		CuentaPlanPensiones c8 = new CuentaPlanPensiones("444444444","titular4",44,4,6.7);
		System.out.println("Muestro datos");
		System.out.println(c8);

	}

}
