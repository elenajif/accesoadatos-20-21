package clases;

public class CuentaPlanPensiones extends Cuenta{
	private double cotizacion;
	
	public CuentaPlanPensiones() {
		super();
		this.interes=3.22;
		this.cotizacion=6.5;
	}
	
	public CuentaPlanPensiones(String numero,String titular, double saldo,double interes,double cotizacion) {
		super(numero, titular, saldo, interes);
		this.interes=interes;
		this.cotizacion=cotizacion;
	}

	public double getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(double cotizacion) {
		this.cotizacion = cotizacion;
	}

	@Override
	public String toString() {
		return "CuentaPlanPensiones "
				+ "cotizacion=" + cotizacion 
				+ ", numero=" + numero 
				+ ", titular=" + titular
				+ ", saldo=" + saldo 
				+ ", interes=" + interes ;
	}

}
