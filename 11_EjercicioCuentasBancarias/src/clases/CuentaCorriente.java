package clases;

public class CuentaCorriente extends Cuenta {
	public CuentaCorriente() {
		super();
	}
	
	public CuentaCorriente(String numero, String titular, double saldo, double interes) {
		super(numero, titular, saldo, interes);
	}
	
	public double reintegro (int perras) {
		saldo-=perras;
		return saldo;
	}
}
