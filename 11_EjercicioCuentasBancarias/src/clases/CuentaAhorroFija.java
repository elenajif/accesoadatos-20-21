package clases;

public class CuentaAhorroFija extends Cuenta {
	public CuentaAhorroFija() {
		super();
		this.interes=2.6;
	}
	public CuentaAhorroFija(String numero,String cuenta, double saldo, double interes) {
		super(numero, cuenta, saldo, interes);
		this.interes=interes;
	}
}
