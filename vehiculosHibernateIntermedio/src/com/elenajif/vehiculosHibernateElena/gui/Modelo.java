package com.elenajif.vehiculosHibernateElena.gui;

        import com.elenajif.vehiculosHibernateElena.Coche;
        import com.elenajif.vehiculosHibernateElena.Propietario;
        import org.hibernate.Session;
        import org.hibernate.SessionFactory;
        import org.hibernate.boot.registry.StandardServiceRegistry;
        import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
        import org.hibernate.cfg.Configuration;

        import javax.persistence.Query;
        import java.util.ArrayList;
        import java.util.List;

public class Modelo {

        SessionFactory sessionFactory;

        public void desconectar() {
            //cierro la factoria de sesiones
            if (sessionFactory!=null && sessionFactory.isOpen())
                sessionFactory.close();
        }

        public void conectar() {
            Configuration configuracion = new Configuration();
            //cargo el fichero hibernate.cfg.xml
            configuracion.configure("hibernate.cfg.xml");

            //indico las clases mapeadas
            configuracion.addAnnotatedClass(Coche.class);
            configuracion.addAnnotatedClass(Propietario.class);

            //creamos un objeto ServiceRegistry para los parametros de configuracion
            //esta clase se encarga de gestionar y proveer servicios
            StandardServiceRegistry ssr= new StandardServiceRegistryBuilder().applySettings(
                    configuracion.getProperties()).build();

            //finalmente creamos un objeto sessionFactory a partir de la configuracion y los registros

            sessionFactory=configuracion.buildSessionFactory(ssr);
        }

        public void altaCoche(Coche nuevoCoche) {
            //obtengo el objeto session
            Session sesion=sessionFactory.openSession();

            sesion.beginTransaction();
            sesion.save(nuevoCoche);
            sesion.getTransaction().commit();

            sesion.close();
        }

        public ArrayList<Coche> getCoches() {
            Session sesion=sessionFactory.openSession();

            Query query =sesion.createQuery("FROM Coche");
            ArrayList<Coche> lista = (ArrayList<Coche>) query.getResultList();

            sesion.close();
            return lista;
        }

        public void modificar(Coche cocheSeleccion) {
            Session sesion=sessionFactory.openSession();

            sesion.beginTransaction();
            sesion.saveOrUpdate(cocheSeleccion);
            sesion.getTransaction().commit();

            sesion.close();
        }

        public void borrar(Coche cocheBorrado) {
            Session session=sessionFactory.openSession();

            session.beginTransaction();
            session.delete(cocheBorrado);
            session.getTransaction().commit();

            session.close();
        }

        public ArrayList<Propietario> getPropietarios() {
            Session sesion=sessionFactory.openSession();

            Query query =sesion.createQuery("FROM Propietario");
            ArrayList<Propietario> listaPropietarios = (ArrayList<Propietario>) query.getResultList();

            sesion.close();
            return listaPropietarios;
        }

    public ArrayList<Coche> getCochesPropietario(Propietario propietarioSeleccionado) {
        Session sesion=sessionFactory.openSession();

        Query query =sesion.createQuery("FROM Coche WHERE propietario:=prop");
        query.setParameter("prop",propietarioSeleccionado);
        ArrayList<Coche> lista = (ArrayList<Coche>) query.getResultList();

        sesion.close();
        return lista;
    }




}
