package com.elenajif.libreriamvc;

import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

/**
 * Created by DAM on 02/12/2020.
 */
public class Vista {
    private JPanel panel1;
    JTextField txtIsbn;
    JTextField txtTitulo;
    JTextField txtAutor;
    JButton btnInsertar;
    JButton btnEliminar;
    JButton btnListar;
    JTable table1;
    DateTimePicker dateTimePicker;

    //creada por mi
    DefaultTableModel dtm;


    public Vista() {
        JFrame frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        iniciarTabla();
    }

    private void iniciarTabla() {
        dtm=new DefaultTableModel();
        table1.setModel(dtm);

        Object[] cabeceras = {"id","isbn","titulo","autor","fecha publicacion"};

        dtm.setColumnIdentifiers(cabeceras);

    }
}
