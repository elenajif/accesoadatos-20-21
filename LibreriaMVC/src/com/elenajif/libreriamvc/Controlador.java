package com.elenajif.libreriamvc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

/**
 * Created by DAM on 02/12/2020.
 */
public class Controlador implements ActionListener {

    private Modelo modelo;
    private Vista vista;

    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;
        anadirActionListeners(this);
        modelo.conectar();
    }

    private void anadirActionListeners(ActionListener listener) {
        vista.btnEliminar.addActionListener(listener);
        vista.btnInsertar.addActionListener(listener);
        vista.btnListar.addActionListener(listener);
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        String comando = e.getActionCommand();

        switch (comando) {
            case "Insertar":
                try {
                    modelo.insertar(vista.txtIsbn.getText(),vista.txtTitulo.getText(),vista.txtAutor.getText(),vista.dateTimePicker.getDateTimePermissive());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
            case "Listar":
                int numero;
                try {
                    modelo.consultarLibros();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
            case "Eliminar":
                try {
                    modelo.eliminar(vista.txtIsbn.getText());
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
                break;
        }
    }
}
