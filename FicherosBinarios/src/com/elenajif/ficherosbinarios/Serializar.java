package com.elenajif.ficherosbinarios;

import java.io.*;

/**
 * Created by DAM on 28/10/2020.
 */
public class Serializar {

    public void escribirObjeto(Object objeto) {
        FileOutputStream fichero = null;
        ObjectOutputStream serializador = null;

        try {
            fichero = new FileOutputStream("archivo.dat");
            serializador = new ObjectOutputStream(fichero);

            serializador.writeObject(objeto);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (serializador != null) {
                try {
                    serializador.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }

    }

    public Object leerObjeto() {
        FileInputStream fichero = null;
        ObjectInputStream serializador = null;
        Object objeto = null;

        try {
            fichero = new FileInputStream("archivo.dat");
            serializador = new ObjectInputStream(fichero);

            objeto = serializador.readObject();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (serializador != null) {
                try {
                    serializador.close();
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }

            return objeto;
        }
    }
}
