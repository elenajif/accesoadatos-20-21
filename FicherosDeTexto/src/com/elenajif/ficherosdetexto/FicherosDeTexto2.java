package com.elenajif.ficherosdetexto;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by DAM on 22/10/2020.
 */
public class FicherosDeTexto2 {
    public static void main(String[] args) throws IOException {
        //escribir con FileWriter
        FileWriter writer = new FileWriter("fichero2.txt");
        writer.write("Traspasamos a fichero \n");
        writer.close();

        //leer fichero de texto plano
        File fichero = new File("fichero2.txt");
        Scanner lector = null;

        lector = new Scanner(fichero);
        while (lector.hasNextLine()) {
            System.out.println(lector.nextLine());
        }
        lector.close();
    }
}
