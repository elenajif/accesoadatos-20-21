package com.elenajif.ficherosdetexto;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by DAM on 23/10/2020.
 */
public class FicherosDeTexto3 {
    public static void main(String[] args) throws IOException {
        //escribir en fichero
        //no va a sobreescribir el fichero destino
        FileWriter writer = new FileWriter("fichero3.txt",true);
        writer.write("Traspaso a fichero \n");
        writer.write("Traspaso a fichero \n");
        writer.write("Traspaso a fichero \n");
        writer.write("Traspaso a fichero \n");
        writer.close();
        //leer fichero de texto plano
        File fichero = new File("fichero3.txt");
        Scanner lector =null;
        lector=new Scanner(fichero);
        while (lector.hasNextLine()) {
            System.out.println(lector.nextLine());
        }
        lector.close();
    }
}
