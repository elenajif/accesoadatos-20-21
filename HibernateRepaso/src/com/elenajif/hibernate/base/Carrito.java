package com.elenajif.hibernate.base;

import javax.persistence.*;
import java.util.List;

/**
 * Created by DAM on 25/01/2021.
 */
@Entity
@Table(name = "carritos", schema = "ejercicio8", catalog = "")
public class Carrito {
    private Usuario usuario;
    private List<DetalleCarrito> detalles;

    @ManyToOne
    @JoinColumn(name = "id_usuario", referencedColumnName = "id", nullable = false)
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @OneToMany(mappedBy = "carrito")
    public List<DetalleCarrito> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<DetalleCarrito> detalles) {
        this.detalles = detalles;
    }
}
