package com.elenajif.hibernate.base;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Created by DAM on 25/01/2021.
 */
@Entity
@Table(name = "usuario_seccion", schema = "ejercicio8", catalog = "")
public class Visita {
    private Usuario usuario;
    private Seccion seccion;

    @ManyToOne
    @JoinColumn(name = "id_usuario", referencedColumnName = "id", nullable = false)
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @ManyToOne
    @JoinColumn(name = "id_seccion", referencedColumnName = "id", nullable = false)
    public Seccion getSeccion() {
        return seccion;
    }

    public void setSeccion(Seccion seccion) {
        this.seccion = seccion;
    }
}
