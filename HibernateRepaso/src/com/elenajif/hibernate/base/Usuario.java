package com.elenajif.hibernate.base;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by DAM on 25/01/2021.
 */
@Entity
@Table(name = "usuarios", schema = "ejercicio8", catalog = "")
public class Usuario {
    private List<Carrito> carritos;
    private List<Visita> visitas;

    @OneToMany(mappedBy = "usuario")
    public List<Carrito> getCarritos() {
        return carritos;
    }

    public void setCarritos(List<Carrito> carritos) {
        this.carritos = carritos;
    }

    @OneToMany(mappedBy = "usuario")
    public List<Visita> getVisitas() {
        return visitas;
    }

    public void setVisitas(List<Visita> visitas) {
        this.visitas = visitas;
    }
}
