package com.elenajif.hibernate.base;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Created by DAM on 25/01/2021.
 */
@Entity
@Table(name = "productos", schema = "ejercicio8", catalog = "")
public class Producto {
    private List<Seccion> secciones;
    private List<DetalleCarrito> detalles;

    @ManyToMany(mappedBy = "productos")
    public List<Seccion> getSecciones() {
        return secciones;
    }

    public void setSecciones(List<Seccion> secciones) {
        this.secciones = secciones;
    }

    @OneToMany(mappedBy = "producto")
    public List<DetalleCarrito> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<DetalleCarrito> detalles) {
        this.detalles = detalles;
    }
}
