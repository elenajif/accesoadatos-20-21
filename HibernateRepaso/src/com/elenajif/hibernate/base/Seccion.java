package com.elenajif.hibernate.base;

import javax.persistence.*;
import java.util.List;

/**
 * Created by DAM on 25/01/2021.
 */
@Entity
@Table(name = "secciones", schema = "ejercicio8", catalog = "")
public class Seccion {
    private List<Visita> visitas;
    private List<Producto> productos;

    @OneToMany(mappedBy = "seccion")
    public List<Visita> getVisitas() {
        return visitas;
    }

    public void setVisitas(List<Visita> visitas) {
        this.visitas = visitas;
    }

    @ManyToMany
    @JoinTable(name = "seccion_producto", catalog = "", schema = "ejercicio8", joinColumns = @JoinColumn(name = "id_producto", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_seccion", referencedColumnName = "id", nullable = false))
    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }
}
