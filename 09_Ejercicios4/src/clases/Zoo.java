package clases;

public class Zoo {
	private Animal[] animales;

	public Zoo(int maxAnimales) {
		this.animales = new Animal[maxAnimales];
	}

	/**
	 * da de alta un nuevo animal
	 * 
	 * @param nombreAnimal que tendra en animal
	 * @param peso         del animal
	 * @param especie      del animal
	 * @param zoo          del animal
	 */

	public void altaAnimal(String nombreAnimal, double peso, String especie, String zoo) {
		for (int i = 0; i < animales.length; i++) {
			if (animales[i] == null) {
				animales[i] = new Animal(nombreAnimal);
				animales[i].setPeso(peso);
				animales[i].setEspecie(especie);
				animales[i].setZoo(zoo);
				break;
			}
		}
	}

	/**
	 * Elimina animales con el codigo introducido
	 * 
	 * @param nombreAnimal nombre del animal que se quiere eliminar
	 */
	public void eliminarAnimal(String nombreAnimal) {
		for (int i = 0; i < animales.length; i++) {
			if (animales[i] != null) {
				if (animales[i].getNombreAnimal().equals(nombreAnimal)) {
					animales[i] = null;
				}
			}
		}
	}

	/**
	 * Pintamos por consola los animales
	 */

	public void listarAnimales() {
		for (int i = 0; i < animales.length; i++) {
			if (animales[i] != null) {
				System.out.println(animales[i]);
			}
		}
	}

	/**
	 * Busca un determinado animal tomando como parámetro el nombre del animal
	 * 
	 * @param nombreAnimal nombre del animal que quiero buscar
	 * @return objeto Animal
	 */

	public Animal buscarAnimal(String nombreAnimal) {
		for (int i = 0; i < animales.length; i++) {
			if (animales[i] != null) {
				if (animales[i].getNombreAnimal().equals(nombreAnimal)) {
					return animales[i];
				}
			}
		}
		return null;
	}

	/**
	 * pinta por consola una lista de animales que contenga el zoo introducido
	 * 
	 * @param zoo zoo del que quiero mostrar los animales
	 */

	public void listarAnimalesPorZoo(String zoo) {
		for (int i = 0; i < animales.length; i++) {
			if (animales[i] != null) {
				if (animales[i].getZoo().equals(zoo)) {
					System.out.println(animales[i]);
				}
			}
		}
	}

	/**
	 * Modifica el nombre de un animal
	 * 
	 * @param nombreAnimal  nombre del animal que quiero cambiar
	 * @param nombreAnimal2 nombre nuevo del animal
	 */

	public void cambiarNombreAnimal(String nombreAnimal, String nombreAnimal2) {
		for (int i = 0; i < animales.length; i++) {
			if (animales[i] != null) {
				if (animales[i].getNombreAnimal().equals(nombreAnimal)) {
					animales[i].setNombreAnimal(nombreAnimal2);
				}
			}
		}
	}

}
