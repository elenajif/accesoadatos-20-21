package programa;

import clases.Zoo;

public class Programa {
	public static void main(String[] args) {
		System.out.println("1.- Crear instancia de Zoo llamada miZoo con 4 animales");
		int maxAnimales = 4;
		Zoo miZoo = new Zoo(maxAnimales);
		System.out.println("Instancia creada");
		
		System.out.println("2.- Damos de alta 4 animales");
		miZoo.altaAnimal("Tim�n", 10, "Suricato", "zoo 1");
		miZoo.altaAnimal("Pumba", 30, "Fac�quero", "zoo 1");
		miZoo.altaAnimal("Simba", 60, "le�n", "zoo 1");
		miZoo.altaAnimal("Kowalski", 20, "Ping�ino", "zoo 2");
		System.out.println("Animales creados");
		
		System.out.println("Listamos animales");
		miZoo.listarAnimales();
		
		System.out.println("3.- Eliminamos animal");
		miZoo.eliminarAnimal("Tim�n");
		
		System.out.println("Listamos animales");
		miZoo.listarAnimales();
		
		System.out.println("4.- Damos de alta un nuevo animal");
		miZoo.altaAnimal("Dori", 1, "pez cirujano", "zoo 2");
		
		System.out.println("Listamos animales");
		miZoo.listarAnimales();
		
		System.out.println("5.- Buscar un animal");
		System.out.println("Buscamos Dori");
		System.out.println(miZoo.buscarAnimal("Dori"));
		
		System.out.println("6.- Listar animales por zoo");
		System.out.println("Listamos animales del zoo 1");
		miZoo.listarAnimalesPorZoo("zoo 1");
		
		System.out.println("7.- Cambiamos de nombre un animal");
		miZoo.cambiarNombreAnimal("Dori", "Hola soy Dori");
		
		System.out.println("Listamos animales");
		miZoo.listarAnimales();
		
		
	}
}
