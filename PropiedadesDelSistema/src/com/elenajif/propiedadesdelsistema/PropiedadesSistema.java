package com.elenajif.propiedadesdelsistema;

import java.io.File;

/**
 * Created by DAM on 21/10/2020.
 */
public class PropiedadesSistema {
    public static void main(String[] args) {
        System.out.println("Caracter separador de rutas");
        System.out.println(File.separator);
        //ALT+INTRO para importar la clase
        System.out.println("Carpeta personal del usuario");
        System.out.println(System.getProperty("user.home"));
        System.out.println("Ruta en la que se encuentra  el usuario");
        System.out.println(System.getProperty("user.dir"));

    }
}
