package com.elenajif.estructurasdatos;

public class Libro {
    private String titulo;
    private String autor;

    public Libro() {
        this.autor="";
        this.titulo="";
    }

    public Libro(String autor, String titulo) {
        this.autor=autor;
        this.titulo=titulo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }


    public void setAutor(String autor) {
        this.autor = autor;
    }

    @Override
    public String toString() {
        return "Libro{" +
                "titulo='" + titulo + '\'' +
                ", autor='" + autor + '\'' +
                '}';
    }

}
