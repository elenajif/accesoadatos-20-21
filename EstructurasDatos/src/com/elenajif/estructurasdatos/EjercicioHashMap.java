package com.elenajif.estructurasdatos;

import java.util.*;

public class EjercicioHashMap {

    // ArrayList, TreeSet y LinkedList implementan el interface Collection
    // TreeSet y HashSet implementan el interface Set

    public static void main(String[] args) {

        HashMap<String, Libro> mapLibros = new HashMap<String,Libro>();
        System.out.println("Insertamos un dato");
        Libro libro = new Libro();
        libro.setTitulo("Secuestrado");
        libro.setAutor("Robert Louis Stevenson");
        mapLibros.put(libro.getTitulo(), libro);

        System.out.println("Mostramos un valor dado un titulo");
        String tituloLibro = "Secuestrado";
        System.out.println(mapLibros.get(tituloLibro));

        System.out.println("Insertamos 2 datos más");
        Libro libro1 = new Libro("El amor en los tiempos del cólera","Gabriel García Márquez");
        mapLibros.put(libro1.getTitulo(), libro1);
        Libro libro2 = new Libro("Don Quijote de la Mancha","Miguel de Cervantes");
        mapLibros.put(libro2.getTitulo(), libro2);

        System.out.println("Obtener una Collection con todos los valores");
        // ArrayList, TreeSet y LinkedList implementan el interface Collection
        System.out.println("Mostramos valores");
        Collection<Libro> coleccionLibros = mapLibros.values();
        for (Libro elemento : coleccionLibros)
            System.out.print(elemento +" ");
        System.out.println();

        System.out.println("Comprobar si existe una clave");
        String titulo = "Secuestrado";
        if (mapLibros.containsKey(titulo)) {
            System.out.println("El libro con el título " + titulo + " existe en tu colección");
        }

        System.out.println("Obtener un Set con todas las claves");
        Set<String> titulos = mapLibros.keySet();
        // TreeSet y HashSet implementan el interface Set

        System.out.println("Comprobar el tamaño del Map");
        System.out.println("Tienes " + mapLibros.size() + " libros en tu colección");

        System.out.println("Concatenar todos los elementos de otro Map");
        Map<String, Libro> masLibros = new HashMap<String, Libro>();

        Libro masLibros1 = new Libro("La montaña mágica","Tomas Mann");
        mapLibros.put(libro.getTitulo(), masLibros1);
        Libro masLibros2 = new Libro("Moby-Dick","Herman Melville");
        mapLibros.put(libro.getTitulo(), masLibros2);
        Libro masLibros3 = new Libro("El cuaderno dorado","Doris Lessing");
        mapLibros.put(libro.getTitulo(), masLibros3);

        mapLibros.putAll(masLibros);

        System.out.println("Mostramos valores");
        for (Libro elemento : coleccionLibros)
            System.out.print(elemento +" ");
        System.out.println();

        System.out.println("Eliminar un elemento (por clave)");
        titulo = "Secuestrado";
        mapLibros.remove(titulo);

        System.out.println("Mostramos valores");
        for (Libro elemento : coleccionLibros)
            System.out.print(elemento +" ");
        System.out.println();

        System.out.println("Eliminar todos los elementos");
        mapLibros.clear();

        System.out.println("Comprobar si esta vacío");
        if (mapLibros.isEmpty()) {
            System.out.println("Tu colección de libros está vacía");
        }

    }
}
