package com.elenajif.estructurasdatos;

import java.util.*;

public class EjercicioTreeSet {

    //TreeSet: Los elementos del conjunto se almacenan de menor a mayor.
    public static void main(String[] args) {
        System.out.println("Creamos 4 productos y añadimos a TreeSet");
        String producto1 = "Pan";
        String producto2 = "Manzanas";
        String producto3 = "Brocoli";
        String producto4 = "Carne";

        // Definir un TreeSet
        Set<String> listaTreeSet = new TreeSet();
        listaTreeSet.add(producto1);
        listaTreeSet.add(producto2);
        listaTreeSet.add(producto3);
        listaTreeSet.add(producto4);

        System.out.println("Mostrar elementos");
        for (Object elemento : listaTreeSet)
            System.out.print(elemento +" ");
        System.out.println();

        System.out.println("Añadir un elemento");
        listaTreeSet.add("lechuga");

        System.out.println("Mostrar elementos");
        for (Object elemento : listaTreeSet)
            System.out.print(elemento +" ");
        System.out.println();

        System.out.println("Añadir todos los elementos de otra Collection");
        List<String> listaArrayList= new ArrayList<>();
        listaArrayList.add("lentejas");
        listaArrayList.add("garbanzos");
        listaArrayList.add("judias");

        listaTreeSet.addAll(listaArrayList);

        System.out.println("Mostrar elementos");
        for (Object elemento : listaTreeSet)
            System.out.print(elemento +" ");
        System.out.println();

        System.out.println("Comprueba si existe un elemento en la lista");
        String texto = "lentejas";
        if (listaTreeSet.contains(texto)) {
            System.out.println("La cadena "+texto+" existe");
        }

        System.out.println("Obtener el primer elemento");
        System.out.println(((TreeSet<String>) listaTreeSet).first());
        System.out.println("Obtener y eliminar el primer elemento");
        ((TreeSet<String>) listaTreeSet).pollFirst();

        System.out.println("Mostrar elementos");
        for (Object elemento : listaTreeSet)
            System.out.print(elemento +" ");
        System.out.println();

        System.out.println("Obtener el último elemento");
        System.out.println(((TreeSet<String>) listaTreeSet).last());
        System.out.println("Obtener y eliminar el último elemento");
        ((TreeSet<String>) listaTreeSet).pollLast();

        System.out.println("Mostrar elementos");
        for (Object elemento : listaTreeSet)
            System.out.print(elemento +" ");
        System.out.println();

        System.out.println("Comprobar el número de elementos");
        System.out.println("Tienes " + listaTreeSet.size() + " cadenas");

        System.out.println("Obtiene la parte del Set cuyos elementos son menores que uno pasado por parámetro");
        String cadenaLimite = "carne";
        System.out.println(((TreeSet<String>) listaTreeSet).headSet(cadenaLimite));
        // SortedSet es el interface que implementa TreeSet

        System.out.println("Obtiene la parte del Set cuyos elementos están entre dos determinados");
        String cadenaMenor = "Pan";
        String cadenaMayor = "lechuga";
        System.out.println(((TreeSet<String>) listaTreeSet).subSet(cadenaMenor, cadenaMayor));
        // subSet puede funcionar también cademaMayor, cadenaMenor

        System.out.println("Eliminar todos los elementos");
        listaTreeSet.clear();

        System.out.println("Comprobar si esta vacío");
        if (listaTreeSet.isEmpty()) {
            System.out.println("El Set de cadenas esta vacío");
        }
    }
}
